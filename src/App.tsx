import React, { Component } from "react";
import { Route } from "react-router-dom";
import { IonApp, IonRouterOutlet, setupConfig } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
// import "@ionic/react/css/padding.css";
// import "@ionic/react/css/float-elements.css";
// import "@ionic/react/css/text-alignment.css";
// import "@ionic/react/css/text-transformation.css";
// import "@ionic/react/css/flex-utils.css";
// import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import "./theme/custom.css";

import Home from "./pages/Home";
import Quiz from "./pages/Quiz";
import { connect } from "react-redux";
import Form from "./pages/Form";
import Completed from "./pages/Completed";
import Format from "./pages/Format";

setupConfig({
	hardwareBackButton: true,
});

export class App extends Component {
	render() {
		return (
			<IonApp>
				<IonReactRouter basename={"/quiz"}>
					<IonRouterOutlet>
						<Route path={`/`} component={Home} exact />
						<Route path={`/form`} component={Form} exact />
						<Route path={`/format`} component={Format} exact />
						<Route path={`/quiz`} component={Quiz} exact />
						<Route
							path={`/completed`}
							component={Completed}
							exact
						/>
					</IonRouterOutlet>
				</IonReactRouter>
			</IonApp>
		);
	}
}

export default connect()(App);
