import { UserState, SAVE_USER_DATA, RESET_USER_STATE } from "./types";
import { DispatchAction } from "../types";
import axios from "axios";
import { host } from "../../config";
import { quizReady, loadQuiz } from "../quiz/actions";

export function saveUserData(state: UserState) {
	return {
		type: SAVE_USER_DATA,
		payload: state,
		metadata: {},
	};
}

export function resetUserState() {
	return {
		type: RESET_USER_STATE,
	};
}

export const sendUserData = (state: UserState): DispatchAction => async (
	dispatch,
	getState
) => {
	dispatch(saveUserData(state));
	axios
		.post(
			host + "saveUserAndLoadQuiz.php",
			{ ...state, submit: true },
			{
				headers: {
					"Content-type": "application/x-www-form-urlencoded",
				},
			}
		)
		.then(res => {
			if (res.status === 201) {
				dispatch(loadQuiz(res.data.quiz));
				dispatch(
					saveUserData({ ...state, userid: Number(res.data.userid) })
				);
				dispatch(quizReady(true));
			} else {
				dispatch(quizReady(false));
				dispatch(
					saveUserData({
						...state,
						errorMsg: "Some Error occured",
					})
				);
			}
			console.log(res);
		})
		.catch(err => {
			console.log(err);
			dispatch(quizReady(false)); // Change to false !!!
			dispatch(
				saveUserData({
					...state,
					errorMsg:
						err.response !== undefined
							? err.response.data.message !== undefined
								? err.response.data.message
								: err.message
							: err.message,
				})
			);
		});
};
