import {
	UserState,
	SAVE_USER_DATA,
	UserActionType,
	RESET_USER_STATE,
} from "./types";

const initialState: UserState = {
	name: "",
	age: 0,
	email: "",
	address: "",
	qualification: "",
	gender: "",
	profession: "",
};

export function userReducer(
	state = initialState,
	action: UserActionType
): UserState {
	switch (action.type) {
		case SAVE_USER_DATA:
			return {
				...state,
				...action.payload,
			};
		case RESET_USER_STATE:
			return initialState;
		default:
			return state;
	}
}
