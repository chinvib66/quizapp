export interface UserState {
    name: string;
    age: number;
    email: string;
    address: string;
    qualification: string;
    gender: string;
    profession: string;
    errorMsg?: string;
    userid?: number;
}

export const SAVE_USER_DATA = "saveUserData";
export const UPDATE_USER_STATE = "updateUserState";
export const RESET_USER_STATE = "resetUserState";

interface UserActionTypes {
    type: string;
    payload: object;
    metadata: object;
}

export type UserActionType = UserActionTypes;
