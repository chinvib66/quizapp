export interface QuizState {
    questions: Question[];
    answers: Answer[];
    score?: number;
    ready?: boolean;
    currentQ: number;
    completed?: boolean;
}
export interface Answer {
    question: number;
    answer: string;
}
export interface Question {
    question: { id: number; value: string; animation: any };
    options: { id: number; value: string }[];
}
export const QUIZ_READY = "quizReady";
export const SAVE_ANSWER = "saveAnswer";
export const EVALUATE_ANSWERS = "evaluateAnswers";
export const SET_CURRENT_QUES = "setCurrentQues";
export const LOAD_QUIZ = "loadQuiz";
export const RESET_QUIZ = "resetQuiz";

interface QuizActionTypes {
    type: string;
    payload: any;
    metadata: object;
}

export type QuizActionType = QuizActionTypes;
