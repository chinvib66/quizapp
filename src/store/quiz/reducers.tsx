import {
    QuizState,
    QuizActionType,
    QUIZ_READY,
    SAVE_ANSWER,
    SET_CURRENT_QUES,
    LOAD_QUIZ,
    RESET_QUIZ
} from "./types";

const initialState: QuizState = {
    questions: [],
    answers: [],
    ready: false,
    score: 0,
    currentQ: 0,
    completed: false
};

export function quizReducer(
    state = initialState,
    action: QuizActionType
): QuizState {
    switch (action.type) {
        case QUIZ_READY:
            return {
                ...state,
                ...action.payload
            };
        case LOAD_QUIZ:
            return {
                ...state,
                ...action.payload
            };
        case SAVE_ANSWER:
            return {
                ...state,
                answers: [
                    ...state.answers,
                    {
                        question: action.payload.question,
                        answer: action.payload.answer
                    }
                ]
            };
        case SET_CURRENT_QUES:
            return {
                ...state,
                ...action.payload
            };
        case RESET_QUIZ:
            return initialState;
        default:
            return state;
    }
}
