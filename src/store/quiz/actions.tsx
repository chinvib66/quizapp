import {
	QUIZ_READY,
	SAVE_ANSWER,
	SET_CURRENT_QUES,
	Answer,
	LOAD_QUIZ,
	Question,
	RESET_QUIZ,
} from "./types";
import { DispatchAction } from "../types";
import axios from "axios";
import { host } from "../../config";
// import { resetUserState } from "../user/actions";

export function quizReady(ready: boolean) {
	return {
		type: QUIZ_READY,
		payload: { ready: ready },
	};
}

export function loadQuiz(state: Question[]) {
	return {
		type: LOAD_QUIZ,
		payload: { questions: state },
	};
}

export function saveAnswer(formState: Answer) {
	return {
		type: SAVE_ANSWER,
		payload: formState,
	};
}

export function setCurrentQues(currentQ: number | null | undefined) {
	if (currentQ === null) {
		return;
	}
	return {
		type: SET_CURRENT_QUES,
		payload: { currentQ: currentQ },
	};
}

export function resetQuiz() {
	return {
		type: RESET_QUIZ,
	};
}

export const submitAnswers = (): DispatchAction => async (
	dispatch,
	getState: any
) => {
	axios
		.post(
			host + "saveQuiz.php",
			{
				userid: getState().user.userid,
				answers: getState().quiz.answers,
			},
			{
				headers: {
					"Content-type": "application/x-www-form-urlencoded",
				},
			}
		)
		.then(res => {
			console.log(res.data);
			// dispatch(quizReady(true));
			// dispatch(resetUserState());
			// dispatch(resetQuiz());
		})
		.catch(err => {
			console.log(err);
		});
};
