import { createStore, combineReducers, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { userReducer } from "./user/reducers";
import { quizReducer } from "./quiz/reducers";

const rootReducer = combineReducers({
    user: userReducer,
    quiz: quizReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
    const middlewares = [thunkMiddleware];
    const middleWareEnhancer = applyMiddleware(...middlewares);

    /* eslint-disable no-underscore-dangle */
    const store = createStore(
        rootReducer,
        composeWithDevTools(middleWareEnhancer)
    );
    /* eslint-enable */
    return store;
}
