import { IonContent, IonPage } from "@ionic/react";
import React, { Component } from "react";

class Completed extends Component {
    render() {
        return (
            <IonPage>
                <IonContent className="ion-padding vertical-align-content">
                    <div>Completed, Thank you</div>
                </IonContent>
            </IonPage>
        );
    }
}

export default Completed;
