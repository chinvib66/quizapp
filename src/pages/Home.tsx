import { IonContent, IonPage } from "@ionic/react";
import React, { Component } from "react";
import { Button, Slide } from "@material-ui/core";

interface HomeState {
	loaded: boolean;
}

class Home extends Component<{}, HomeState> {
	constructor(props: {}) {
		super(props);
		this.state = {
			loaded: false,
		};
	}

	componentDidMount() {
		this.setState({ loaded: true });
	}

	render() {
		return (
			<IonPage>
				<IonContent className='ion-padding vertical-align-content'>
					<div style={{ display: "flex", flexDirection: "column" }}>
						<Slide
							direction='up'
							in={this.state.loaded}
							timeout={3000}
						>
							<div className='opening-animation'>
								<img
									alt='animation'
									src={require("../assets/cardb.png")}
								/>
							</div>
						</Slide>
						<div className='quiz-format heading'>Quiz Time</div>
						<Button
							className='get-started-button'
							style={{ marginTop: "30px" }}
							href={`${process.env.PUBLIC_URL}/form`}
							// href='/form'
						>
							Get Started
						</Button>
					</div>
				</IonContent>
			</IonPage>
		);
	}
}

export default Home;
