import { IonPage, IonContent } from "@ionic/react";
import React, { Component } from "react";
import {
	TextField,
	Grid,
	Container,
	Button,
	RadioGroup,
	FormLabel,
	Radio,
} from "@material-ui/core";
// import CloseIcon from "@material-ui/icons/Close";

import { AppState } from "../store";
import { QuizState } from "../store/quiz/types";
import { UserState } from "../store/user/types";
import { connect } from "react-redux";
import { History, LocationState } from "history";

import { sendUserData } from "../store/user/actions";
import { quizReady } from "../store/quiz/actions";

export interface FormProps {
	quiz: QuizState;
	user: UserState;
	sendUserData: any;
	quizReady: typeof quizReady;
	history: History<LocationState>;
}

interface FormState extends UserState {}

class Form extends Component<FormProps, FormState> {
	constructor(props: FormProps) {
		super(props);
		this.state = {
			name: "",
			age: 0,
			email: "",
			address: "",
			qualification: "",
			gender: "",
			profession: "",
		};
		this.submitHandler = this.submitHandler.bind(this);
	}

	componentDidUpdate(prevProps: FormProps) {
		if (
			this.props.quiz.ready === true &&
			this.props.quiz.ready !== prevProps.quiz.ready
		)
			// this.props.history.push(`${process.env.PUBLIC_URL}/format`);
			this.props.history.push("/format");
	}

	submitHandler(e: any) {
		e.preventDefault();
		//set AppState user data
		this.props.sendUserData(this.state).then((stat: any) => {
			// this.props.history.push("/format");
			console.log(stat);
		});
	}
	render() {
		return (
			<IonPage>
				<IonContent>
					<Container
						maxWidth='sm'
						style={{
							alignItems: "center",
							height: "100%",
						}}
					>
						<div className='form-title'>
							<span
								className='title'
								style={{ fontSize: 27, color: "#00afff" }}
							>
								Let's Get Started
							</span>
							<span className='fsubtitle'>
								Enter your details to enter the quiz
							</span>
						</div>
						<form onSubmit={this.submitHandler}>
							<Grid
								container
								direction='row'
								alignItems='center'
								style={{
									textAlign: "center",
									padding: " auto 5px auto px",
								}}
								spacing={1}
							>
								<Grid item xs={12} sm={12}>
									<TextField
										variant='outlined'
										margin='normal'
										type='text'
										name='name'
										label='Name'
										id='name'
										value={this.state.name}
										disabled={this.props.quiz.ready}
										onChange={e =>
											this.setState({
												name: e.target.value,
											})
										}
									/>
								</Grid>
								<Grid item xs={4} md={4}>
									<TextField
										variant='outlined'
										margin='normal'
										type='text'
										name='age'
										label='Age'
										id='age'
										value={
											this.state.age !== 0
												? this.state.age
												: ""
										}
										disabled={this.props.quiz.ready}
										onChange={e =>
											this.setState({
												age: Number(e.target.value),
											})
										}
									/>
								</Grid>
								<Grid container style={{ padding: "4px" }}>
									<Grid item xs={12} sm={12}>
										<RadioGroup
											aria-label='Gender'
											name='gender'
											style={{
												display: "flex",
												flexDirection: "row",
												flexWrap: "nowrap",
											}}
											className='custom-label user-form vertical-align-content'
											onChange={e =>
												this.setState({
													gender: e.target.value,
												})
											}
										>
											<FormLabel
												style={{ width: "120px" }}
												className='gender-label'
											>
												<Radio
													value='Male'
													style={{ display: "none" }}
													className='user-form-radio'
												/>
												<span className='glabel male'>
													Male
												</span>
											</FormLabel>
											<FormLabel
												style={{ width: "120px" }}
												className='gender-label'
											>
												<Radio
													style={{ display: "none" }}
													value='Female'
												/>
												<span className='glabel female'>
													Female
												</span>
											</FormLabel>
										</RadioGroup>
									</Grid>
								</Grid>
								{/* </Grid> */}
								<Grid
									container
									direction='row'
									justify='center'
									alignItems='center'
									style={{ textAlign: "center" }}
									spacing={1}
								>
									<Grid item xs={12} sm={12}>
										<TextField
											variant='outlined'
											margin='normal'
											type='email'
											name='email'
											label='Email'
											id='email'
											value={this.state.email}
											disabled={this.props.quiz.ready}
											onChange={e =>
												this.setState({
													email: e.target.value,
												})
											}
										/>
									</Grid>
									<Grid item xs={12} sm={12}>
										<TextField
											variant='outlined'
											margin='normal'
											type='text'
											name='address'
											label='Address'
											id='address'
											value={this.state.address}
											disabled={this.props.quiz.ready}
											onChange={e =>
												this.setState({
													address: e.target.value,
												})
											}
										/>
									</Grid>
									<Grid item xs={12} sm={12}>
										<TextField
											variant='outlined'
											margin='normal'
											name='qualification'
											label='Qualification'
											id='qualification'
											value={this.state.qualification}
											disabled={this.props.quiz.ready}
											onChange={e =>
												this.setState({
													qualification:
														e.target.value,
												})
											}
										/>
									</Grid>
									<Grid item xs={12} sm={12}>
										<TextField
											variant='outlined'
											margin='normal'
											name='profession'
											label='Profession'
											id='profession'
											value={this.state.profession}
											disabled={this.props.quiz.ready}
											onChange={e =>
												this.setState({
													profession: e.target.value,
												})
											}
										/>
									</Grid>
								</Grid>
								<Grid
									item
									xs={12}
									style={{ marginTop: "20px" }}
									className='textAlignCenter'
								>
									{this.props.user.errorMsg}
								</Grid>
								<Grid
									item
									xs={12}
									style={{ marginTop: "30px" }}
									className='textAlignCenter'
								>
									<Button
										disabled={this.props.quiz.ready}
										onClick={this.submitHandler}
										className='next to-format'
									>
										Next
									</Button>
								</Grid>
							</Grid>
						</form>
					</Container>
				</IonContent>
			</IonPage>
		);
	}
}

const mapStateToProps = (state: AppState) => ({
	quiz: state.quiz,
	user: state.user,
});

const mapActionToProps = {
	sendUserData,
	quizReady,
};

export default connect(
	mapStateToProps,
	mapActionToProps
)(Form);
