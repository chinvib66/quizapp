import { IonPage, IonContent } from "@ionic/react";
import React, { Component } from "react";
import { Container, Grid, Button } from "@material-ui/core";
import { LocationState, History } from "history";
import { connect } from "react-redux";
import { AppState } from "../store";
import { QuizState } from "../store/quiz/types";
import { UserState } from "../store/user/types";
import { Redirect } from "react-router";

interface FormatComponentProps {
	history: History<LocationState>;
	quiz: QuizState;
	user: UserState;
}

class Format extends Component<FormatComponentProps> {
	render() {
		if (!this.props.quiz.ready || !this.props.user.userid) {
			// this.props.history.push(`${process.env.PUBLIC_URL}/form`);
			// return <Redirect to={`${process.env.PUBLIC_URL}/form`} />;
			// this.props.history.push(`/form`);
			return <Redirect to={`/form`} />;
		}
		return (
			<IonPage>
				<IonContent>
					<div
						className='vertical-align-content'
						style={{ height: "100%" }}
					>
						<Container
							style={{ textAlign: "center" }}
							maxWidth='sm'
						>
							<Grid container justify='center'>
								<Grid
									className='quiz-format heading'
									item
									xs={12}
									sm={12}
								>
									Quiz Format
								</Grid>
								<Grid
									style={{
										margin: "20px auto 30px auto",
									}}
									item
									xs={12}
									sm={12}
								>
									<Grid
										style={{
											padding: 20,
											margin: "25px auto",
										}}
										container
									>
										<Grid item xs={3} sm={3}>
											<img
												className='play-image'
												alt='play'
												src={require("../assets/play.png")}
											/>
										</Grid>
										<Grid
											item
											style={{ textAlign: "left" }}
											xs={9}
											sm={9}
										>
											<div className='subtitle'>
												Animation
											</div>
											<div>
												First, an engaging animation
												will be shown to you for a few
												seconds.
											</div>
										</Grid>
									</Grid>
									<Grid
										style={{
											padding: 20,
											margin: "25px auto",
										}}
										container
									>
										<Grid item xs={3} sm={3}>
											<img
												className='q-image'
												alt='question'
												src={require("../assets/qmark.png")}
											/>
										</Grid>
										<Grid
											item
											style={{ textAlign: "left" }}
											xs={9}
											sm={9}
										>
											<div className='subtitle'>
												Question
											</div>
											<div>
												After the animation, an exciting
												question based on the animation
												follows.
											</div>
										</Grid>
									</Grid>
								</Grid>
								<Grid
									style={{ marginTop: "70px" }}
									xs={12}
									item
									sm={12}
								>
									<Button
										className='custom-button'
										onClick={() => {
											// this.props.history.push(
											// 	`${process.env.PUBLIC_URL}/quiz`
											// );
											this.props.history.push("/quiz");
										}}
									>
										Got it, Start Quiz
									</Button>
								</Grid>
							</Grid>
						</Container>
					</div>
				</IonContent>
			</IonPage>
		);
	}
}
const mapStateToProps = (state: AppState) => ({
	quiz: state.quiz,
	user: state.user,
});

export default connect(mapStateToProps)(Format);
