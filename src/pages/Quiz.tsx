import { IonContent, IonPage, IonModal } from "@ionic/react";
import React, { Component, Fragment, ChangeEvent } from "react";
import { connect } from "react-redux";
import { History, LocationState } from "history";
import {
	Grid,
	Radio,
	RadioGroup,
	Button,
	// LinearProgress,
	FormLabel,
} from "@material-ui/core";
// import { Redirect } from "react-router";

import { AppState } from "../store";
import { QuizState } from "../store/quiz/types";
import { UserState } from "../store/user/types";
import {
	saveAnswer,
	setCurrentQues,
	submitAnswers,
} from "../store/quiz/actions";
// import { host } from "../config";

function shuffle(array: any[]) {
	array.sort(() => Math.random() - 0.5);
}
/**
 *
 */

interface QuizAppProps {
	quiz: QuizState;
	user: UserState;
	message: string;
	saveAnswer: typeof saveAnswer;
	setCurrentQues: typeof setCurrentQues;
	history: History<LocationState>;
	submitAnswers: any; // Async Action :)!
}

interface QuizAppState extends Omit<QuizState, "answers"> {
	showModal: boolean;
	visited: number[];
	progress: number;
	totalq: number;
	qno: number;
	imgLoaded: boolean;
}

class Quiz extends Component<QuizAppProps, QuizAppState> {
	constructor(props: QuizAppProps) {
		super(props);
		this.state = {
			currentQ: this.props.quiz.currentQ,
			questions: this.props.quiz.questions,
			showModal: false,
			visited: [],
			completed: false,
			progress: 0,
			qno: 1,
			totalq: this.props.quiz.questions.length,
			imgLoaded: false,
		};
		this._displayModal = this._displayModal.bind(this);
		this._selectQ = this._selectQ.bind(this);
		this.changeQuestion = this.changeQuestion.bind(this);
	}

	// componentWillMount() {
	// 	if (this.props.quiz.ready !== true) {
	// 		// this.props.history.push(`${process.env.PUBLIC_URL}/form`);
	// 		this.props.history.push("/form");
	// 	}
	// }
	componentDidMount() {
		this.setState({
			visited: Object.keys(this.state.questions).map(Number),
		});

		// setTimeout(() => this._displayModal(true), 13000);
	}
	componentDidUpdate(prevProps: QuizAppProps, prevState: QuizAppState) {
		if (
			prevState.currentQ !== this.state.currentQ &&
			!this.state.completed
		) {
			this.setState({ ...this.state, imgLoaded: false });
		}
		if (
			this.state.imgLoaded &&
			prevState.imgLoaded !== this.state.imgLoaded
		) {
			setTimeout(() => {
				this._displayModal(true);
			}, 14000);
		}
	}

	// componentDidCatch(error: any) {
	// 	this.setState({ hasError: true });
	// }

	_displayModal(showModal: boolean) {
		this.setState({ showModal: showModal });
	}

	_selectQ(key: number) {
		this.props.setCurrentQues(key);
	}

	changeQuestion() {
		this.setState({ showModal: false, qno: this.state.qno + 1 });
		let visited = this.state.visited.filter((value: number) => {
			return value !== this.state.currentQ;
		});
		if (visited.length !== 0 && this.state.completed !== true) {
			shuffle(visited);
			this.setState(
				{
					visited: visited,
				},
				() =>
					this.setState({ currentQ: this.state.visited[0] }, () =>
						this.props.setCurrentQues(this.state.currentQ)
					)
			);
		} else {
			this.props.submitAnswers();
			this.setState({ completed: true, showModal: false });
			// this.props.history.push(`${process.env.PUBLIC_URL}/completed`);
			this.props.history.push("/completed");
		}
	}

	renderModal() {
		// if (this.state.currentQ !== null) {
		let current = Number(this.state.currentQ);
		return (
			<IonModal isOpen={this.state.showModal} backdropDismiss={false}>
				<QuizForm
					question={this.state.questions[current].question}
					options={this.state.questions[current].options}
					saveAnswer={this.props.saveAnswer}
					changeQ={this.changeQuestion}
					modal={this.state.showModal}
					totalq={this.state.totalq}
					qno={this.state.qno}
				/>
			</IonModal>
		);
		// }
	}
	render() {
		// if (this.state.hasError) {
		// 	this.props.history.push("/form");
		// 	return;
		// }
		// console.log(process.env.PUBLIC_URL);
		// if (
		// 	this.props.user.userid === null ||
		// 	this.props.user.userid === undefined //&&
		// 	// process.env.PUBLIC_URL !== "/completed"
		// ) {
		// 	// return <Redirect to={`${process.env.PUBLIC_URL}/form`} />;
		// 	return <Redirect to={"/form"} />;
		// } else {
		return (
			<IonPage>
				{/* <LinearDeterminate value={this.state.progress} /> */}
				<small
					style={{
						width: "100%",
						color: "#d8d7d7",
						margin: "20px auto auto 20px",
					}}
				>
					Question {this.state.qno} / {this.state.totalq}
				</small>
				<IonContent className='ion-padding vertical-align-content '>
					<Grid container style={{ textAlign: "center" }}>
						<Grid item xs={12}>
							<img
								src={
									this.state.questions[this.state.currentQ]
										.question.animation
								}
								onLoad={() =>
									this.setState({ imgLoaded: true })
								}
								// onChange
								className='quiz-image'
								style={{
									display: this.state.imgLoaded
										? "initial"
										: "none",
								}}
								alt='Quiz Question'
							/>
							<img
								src={require("../assets/loading.gif")}
								alt='Loading'
								style={{
									display: this.state.imgLoaded
										? "none"
										: "initial",
									maxHeight: "100px",
								}}
								className='loading-image'
							/>
						</Grid>
						{/* <Grid item xs={12}>
                                {
                                    this.state.questions[this.state.currentQ]
                                        .question.value
                                }
                            </Grid> */}
					</Grid>
					{this.renderModal()}
				</IonContent>
			</IonPage>
		);
	}
	// }
}

const mapStateToProps = (state: AppState) => ({
	quiz: state.quiz,
	user: state.user,
});

const mapActionToProps = { saveAnswer, setCurrentQues, submitAnswers };

export default connect(
	mapStateToProps,
	mapActionToProps
)(Quiz);

/**
 *
 */

interface QuizFormProps {
	question: { id: number; value: string; animation: any };
	options: { id: number; value: string }[];
	modal?: boolean;
	saveAnswer: typeof saveAnswer;
	changeQ: Function;
	totalq: number;
	qno?: number;
}

interface QuizFormState {
	question: number;
	answer: any;
	button?: boolean;
}

class QuizForm extends Component<QuizFormProps, QuizFormState> {
	constructor(props: QuizFormProps) {
		super(props);
		this.state = {
			question: this.props.question.id,
			answer: null,
			button: this.props.modal,
		};
		this.changeHandler = this.changeHandler.bind(this);
		this.submitHandler = this.submitHandler.bind(this);
		shuffle(this.props.options);
	}

	changeHandler(event: ChangeEvent<HTMLInputElement>) {
		this.setState({ answer: event.currentTarget.value, button: true });
	}

	componentWillReceiveProps() {
		this.setState({
			question: this.props.question.id,
			answer: null,
		});
	}
	componentDidUpdate(prevProps: QuizFormProps, prevState: QuizFormState) {
		if (prevState.question !== this.state.question) {
			shuffle(this.props.options);
		}
	}

	submitHandler(event: any) {
		// event.preventDefault();
		if (!this.state.answer) alert("Select an option");
		else {
			this.props.saveAnswer(this.state);
			this.props.changeQ();
			this.setState({ button: false });
		}
	}
	render() {
		const { question, options } = this.props;
		return (
			<Fragment>
				<div
					className='vertical-align-content'
					style={{
						height: "100%",
						display: this.props.modal ? "block" : "none",
					}}
				>
					<form
						style={{
							flexDirection: "column",
							maxWidth: "80%",
							margin: "auto ",
						}}
					>
						<small
							style={{
								width: "100%",
								color: "#00afff",
								marginBottom: "20px",
							}}
						>
							Question {this.props.qno} / {this.props.totalq}
						</small>

						<div
							style={{ width: "100%" }}
							className='modal-question'
						>
							{question.value}
						</div>

						<RadioGroup
							aria-label='Options'
							name='answer'
							style={{
								width: "100%",
								alignContent: "center",
							}}
							onChange={this.changeHandler}
						>
							{Object.keys(options).map((key, index) => {
								return (
									<FormLabel
										key={key}
										className='custom-label'
									>
										<Radio
											className='custom-radio quiz'
											value={options[index].value}
										/>
										<span className='options-span'>
											{options[index].value}{" "}
										</span>
									</FormLabel>
								);
							})}
						</RadioGroup>
						<Button
							onClick={event => {
								this.setState({ question: question.id }, () =>
									this.submitHandler(event)
								);
							}}
							className='next-button'
							style={{
								backgroundColor: this.state.button
									? "#00afff"
									: "#d8d7d7",
								color: this.state.button ? "white" : "grey",
							}}
						>
							Next
						</Button>
					</form>
				</div>
			</Fragment>
		);
	}
}

// class LinearDeterminate extends Component<
//     { value: number },
//     { value: number }
// > {
//     constructor(props: { value: number }) {
//         super(props);
//         this.state = {
//             value: this.props.value
//         };
//     }
//     componentDidUpdate(prevProps: { value: number }) {
//         if (prevProps.value !== this.props.value) {
//             this.setState({
//                 value: this.props.value
//             });
//         }
//     }

//     render() {
//         return (
//             <div>
//                 <LinearProgress
//                     variant="determinate"
//                     value={this.state.value}
//                 />
//             </div>
//         );
//     }
// }

// function LinearDet() {
//     const [completed, setCompleted] = React.useState(0);

//     React.useEffect(() => {
//         function progress() {
//             setCompleted(prevCompleted =>
//                 prevCompleted >= 100 ? 0 : prevCompleted + 20
//             );
//         }

//         const timer = setInterval(progress, 1000);
//         return () => {
//             clearInterval(timer);
//         };
//     }, []);

//     return (
//         <div>
//             <LinearProgress variant="determinate" value={completed} />
//         </div>
//     );
// }
